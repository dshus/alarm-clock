import java.util.Scanner;
import alarm.AlarmClock;
import tinshoes.input.SafeScanner;
//Not entirely safe because I'm tired
public class Main {
	public static void main(String[] args) {
		String err = " is not valid input.";
		Scanner usc = new Scanner(System.in);
		SafeScanner sc = new SafeScanner(usc);
		System.out.println("Setting default clock...");
		AlarmClock ca = new AlarmClock();
		System.out.println("What sound should the alarm clock make?");
		String sound = usc.nextLine();
		AlarmClock cb = new AlarmClock(sc.nextInt("Enter the hours.", err, true), sc.nextInt("Enter the minutes.", err, true), sc.nextInt("Enter the seconds.", err, true), sc.yn("Should the clock use military time?", "That is not valid input."), sound);
		System.out.println(ca);
		System.out.println(cb);
		ca.check();
		cb.check();
		System.out.println("Setting time for Clock 1...");
		ca.setTime(sc.nextInt("Enter the hours.", err, true), sc.nextInt("Enter the minutes.", err, true), sc.nextInt("Enter the seconds.", err, true));
		System.out.println("Setting time for Clock 2...");
		cb.setTime(sc.nextInt("Enter the hours.", err, true), sc.nextInt("Enter the minutes.", err, true), sc.nextInt("Enter the seconds.", err, true));
		System.out.println(ca);
		System.out.println(cb);
		ca.check();
		cb.check();
	}
}