package alarm;
import java.time.LocalDateTime;
public class AlarmClock {
	private int hours, minutes, seconds;
	private boolean military;
	private String sound;
	public AlarmClock() {
		this(7, 0, 0, false, "Beep!");
	}
	//Assumes time is ENTERED in military time
	public AlarmClock(int hours, int minutes, int seconds, boolean military, String sound) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
		this.military = military;
		this.sound = sound;
	}
	public void setTime(int hours, int minutes, int seconds) {
		this.hours = hours;
		this.minutes = minutes;
		this.seconds = seconds;
	}
	public void play() {
		for (int i = 0; i < 3; i++)
			System.out.println(sound);
	}
	public void check() {
		LocalDateTime now = LocalDateTime.now();
		if (this.hours < now.getHour() || (this.hours == now.getHour() && this.minutes < now.getMinute()) || (this.hours == now.getHour() && this.minutes == now.getMinute() && this.seconds < now.getSecond()))
			this.play();
		else
			System.out.println("The alarm has not gone off yet.");
	}
	public String getSound() {
		return this.sound;
	}
	public void setSound(String sound) {
		this.sound = sound;
	}
	public void setMilitary(boolean military) {
		this.military = military;
	}
	public boolean isMilitary() {
		return this.military;
	}
	public String getTime() {
		return (this.military ? this.hours : (this.hours > 12 ? this.hours - 12 : this.hours)) + ":" + (Integer.toString(minutes).length() < 2 ? "0" + minutes : minutes) + ":" + (Integer.toString(seconds).length() < 2 ? "0" + seconds : seconds) + (this.military ? "" : (this.hours > 12 ? " PM" : " AM"));
	}
	public String toString() {
		return "Rings at " + (this.military ? this.hours : (this.hours > 12 ? this.hours - 12 : this.hours)) + ":" + (Integer.toString(minutes).length() < 2 ? "0" + minutes : minutes) + ":" + (Integer.toString(seconds).length() < 2 ? "0" + seconds : seconds) + (this.military ? "" : (this.hours > 12 ? " PM" : " AM")) + ". Will ring with \"" + sound + "\".";
	}
}